from random import randint

player_name = input("Hi! What is your name? ")

#initializing the bounds for the year guess
earliest_year = 1924
latest_year = 2004

#initial guess of computer and guess number

guess_number = 1

for guess_number in range(5):
    #initial guess of computer and guess number
    guess_month = randint(1,12)
    guess_year = randint(earliest_year,latest_year)
    print("Guess", guess_number +1,":", player_name, "were you born in ", guess_month, "/", guess_year)
    is_guess_correct = input("yes/earlier/later? ")
    if is_guess_correct == "yes":
        print("I knew it")
        exit()
    elif is_guess_correct == "earlier" and guess_number <4:
        print("Drat! Lemme try again!")
        latest_year = guess_year-1
        guess_number +=1
    elif is_guess_correct == "later" and guess_number <4:
        print("Drat! Lemme try again!")
        earliest_year = guess_year+1
        guess_number +=1

print("I have better things to do.")
